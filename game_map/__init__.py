# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import collections
import os
import enum
import math


VERSION_LIST = (
    'v_ravensburger_new',
    'v_ravensburger_old',
)
_COORDINATES_FILENAME = 'map-coordinates-of-stations.txt'
_CONNECTIONS_FILENAME = 'map-connections.txt'
_START_ST_DETECTIVES_FILENAME = 'map-start-stations-detectives.txt'
_START_ST_THIEF_FILENAME = 'map-start-stations-thief.txt'
_CONFIG_FILENAME = 'config.txt'
_CONFIG_THIEF_SHOW_ROUNDS = 'THIEF_SHOW_ROUNDS'
_CONFIG_X_AXIS_NUM_CELLS = 'X_AXIS_NUM_CELLS'
_CONFIG_Y_AXIS_NUM_CELLS = 'Y_AXIS_NUM_CELLS'


@enum.unique
class Transport(enum.Enum):
    ANY = '*'
    TAXI = 'T'
    BUS = 'B'
    SUBWAY = 'S'
    FERRY = 'F'

    def __ge__(self, other):
        if self.__class__ is other.__class__:
            return self.get_value_for_sorting(self) >= self.get_value_for_sorting(other)
        return NotImplemented

    def __gt__(self, other):
        if self.__class__ is other.__class__:
            return self.get_value_for_sorting(self) > self.get_value_for_sorting(other)
        return NotImplemented

    def __le__(self, other):
        if self.__class__ is other.__class__:
            return self.get_value_for_sorting(self) <= self.get_value_for_sorting(other)
        return NotImplemented

    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.get_value_for_sorting(self) < self.get_value_for_sorting(other)
        return NotImplemented

    @classmethod
    def get_value_for_sorting(cls, member):
        key = '_member_sort_values'
        val = getattr(cls, key, None)
        if val is None:
            val = list(cls)
            setattr(cls, key, val)

        return val.index(member)


def _read_coordinates_from_file(version, perform_checks):
    file_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        version,
        _COORDINATES_FILENAME)
    coordinates_list = []

    with open(file_path) as f:
        for i, line in enumerate(f):
            if not line.startswith('#'):        # skip comment lines
                line = line.strip()             # remove spaces and newlines
                if len(line) > 0:               # skip empty lines
                    try:
                        st_number, x, y = line.split()
                        st_number = int(st_number)
                        x = int(x)
                        y = int(y)
                    except ValueError as err:
                        raise ValueError(
                            'unexpected content in file {} on line {}'.format(
                                file_path, i+1)) \
                            .with_traceback(err.__traceback__)

                    coordinates_list.append((st_number, x, y))

    if perform_checks:
        if len(coordinates_list) != len(set(coordinates_list)):
            raise ValueError('there are duplicate map coordinates')

    return coordinates_list


def _read_connections_from_file(version, perform_checks):
    file_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        version,
        _CONNECTIONS_FILENAME)
    connection_list = []

    with open(file_path) as f:
        for i, line in enumerate(f):
            if not line.startswith('#'):        # skip comment lines
                line = line.strip()             # remove spaces and newlines
                if len(line) > 0:               # skip empty lines
                    try:
                        st_orig, tr, st_dest = line.split()
                        st_orig = int(st_orig)
                        st_dest = int(st_dest)
                        tr = Transport(tr)
                    except ValueError as err:
                        raise ValueError(
                            'unexpected content in file {} on line {}'.format(
                                file_path, i+1)) \
                            .with_traceback(err.__traceback__)

                    connection_list.append((st_orig, tr, st_dest))

    if perform_checks:
        if len(connection_list) != len(set(connection_list)):
            raise ValueError('there are duplicate map connections')

        for elem, elem_sorted in zip(connection_list, sorted(connection_list)):
            if elem != elem_sorted:
                raise ValueError('map connection {} is not sorted correctly'.format(elem))
            if elem[0] > elem[2]:
                raise ValueError('map connection {} is invalid'.format(elem))

    return connection_list


def _read_start_stations_from_file(team, version, perform_checks):
    team = team[:1].upper().strip()             # only first char; make uppercase
    file_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        version,
        _START_ST_DETECTIVES_FILENAME if team == 'D' else _START_ST_THIEF_FILENAME)
    station_list = []

    with open(file_path) as f:
        for i, line in enumerate(f):
            if not line.startswith('#'):        # skip comment lines
                line = line.strip()             # remove spaces and newlines
                if len(line) > 0:               # skip empty lines
                    try:
                        st_number = int(line)
                    except ValueError as err:
                        raise ValueError(
                            'unexpected content in file {} on line {}'.format(
                                file_path, i+1)) \
                            .with_traceback(err.__traceback__)

                    station_list.append(st_number)

    if perform_checks:
        if len(station_list) != len(set(station_list)):
            raise ValueError('there are duplicate start stations')

        for elem, elem_sorted in zip(station_list, sorted(station_list)):
            if elem != elem_sorted:
                raise ValueError('start station {} is not sorted correctly'.format(elem))

    return station_list


def _read_config_from_file(version):
    file_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        version,
        _CONFIG_FILENAME)
    config = {}

    with open(file_path) as f:
        for i, line in enumerate(f):
            if not line.startswith('#'):        # skip comment lines
                line = line.strip()             # remove spaces and newlines
                if len(line) > 0:               # skip empty lines
                    try:
                        k, *v_list = line.upper().split()

                        if k == _CONFIG_THIEF_SHOW_ROUNDS:
                            v = [int(v) for v in v_list]
                        elif k in (_CONFIG_X_AXIS_NUM_CELLS, _CONFIG_Y_AXIS_NUM_CELLS):
                            v = int(v_list[0])
                        else:
                            continue            # skip unknown parameter
                    except (ValueError, IndexError) as err:
                        raise ValueError(
                            'unexpected content in file {} on line {}'.format(
                                file_path, i+1)) \
                            .with_traceback(err.__traceback__)

                    config[k] = v

    return config


class _Station:
    def __init__(self, number, x, y):
        self.number = number
        self.map_x = x
        self.map_y = y
        self.connected_stations = collections.defaultdict(set)

    def __str__(self):
        # print information about what transports can be found at this station
        all_transports_of_this_station = set(
            tr
            for tr_set in self.connected_stations.values()
            for tr in tr_set)
        return '{:3d} ({})'.format(
            self.number,
            ''.join(
                tr.value if tr in all_transports_of_this_station else ' '
                for tr in Transport))

    def add_connection(self, st_number, tr):
        # used for building the map; adds the information of the connected station and the
        # transport
        if self.number == st_number:
            raise ValueError('a station cannot have a connection to itself')
        if tr == Transport.ANY:
            raise ValueError('the transport cannot be "{}"'.format(tr.name))

        self.connected_stations[st_number].add(tr)

    def get_connected_stations(self, allowed_tr=Transport.ANY):
        # get a set of stations that can be reached from this station; the result can be
        # filtered by a subset of transports
        if allowed_tr == Transport.ANY:
            allowed_tr_set = set(list(Transport)[1:])   # all except 'ANY'
        elif isinstance(allowed_tr, Transport):
            allowed_tr_set = {allowed_tr, }
        else:
            allowed_tr_set = set(allowed_tr)

        return {
            st_number
            for st_number, tr_set in self.connected_stations.items()
            if len(tr_set & allowed_tr_set) > 0}


class _Map:
    def __init__(self, version, perform_checks):
        self.stations = {
            st_number: _Station(st_number, x, y)
            for st_number, x, y in _read_coordinates_from_file(
                version=version,
                perform_checks=perform_checks)}

        for st_orig, tr, st_dest in _read_connections_from_file(
                version=version,
                perform_checks=perform_checks):
            # the connections are bi-directional, so we add them to both stations
            self.stations[st_orig].add_connection(st_dest, tr)
            self.stations[st_dest].add_connection(st_orig, tr)

        self.start_stations_for_detectives = _read_start_stations_from_file(
            team='D',
            version=version,
            perform_checks=perform_checks)
        self.start_stations_for_thief = _read_start_stations_from_file(
            team='T',
            version=version,
            perform_checks=perform_checks)

        config = _read_config_from_file(version=version)
        self.thief_show_rounds = config[_CONFIG_THIEF_SHOW_ROUNDS]
        self.thief_max_rounds = self.thief_show_rounds[-1]
        self._x_axis_num_cells = config[_CONFIG_X_AXIS_NUM_CELLS]
        self._y_axis_num_cells = config[_CONFIG_Y_AXIS_NUM_CELLS]

        self.travel_costs = self._calculate_travel_costs()

    def __str__(self):
        return 'Stations of the map:\n{}'.format(
            '\n'.join(
                '{}  ==>  {}'.format(
                    str(st),
                    ' | '.join(
                        '{:3d}:{}'.format(
                            st_number,
                            ''.join(
                                tr.value if tr in tr_set else ' '
                                for tr in Transport))
                        for st_number, tr_set in sorted(st.connected_stations.items())))
                for _, st in sorted(self.stations.items())))

    def has_connection(self, st_orig, st_dest, tr=Transport.ANY):
        # checks if the destination can be reached from the origin; the result can be
        # filtered by a subset of transports
        return st_dest in self.stations[st_orig].get_connected_stations(tr)

    def print_map(self, stations_to_show, default_marker='o'):
        # create empty map
        mtx = [
            [[] for _ in range(self._x_axis_num_cells)]
            for _ in range(self._y_axis_num_cells)]

        default_marker = str(default_marker).strip()
        if len(default_marker) != 1:
            raise ValueError('map marker has to be only 1 char')

        if not isinstance(stations_to_show, dict):
            # transform into a 'dict'
            stations_to_show = {
                st_number: default_marker
                for st_number in stations_to_show}

        # add markers to the map; we use '.append()' because there could be multiple
        # markers at a single station
        for st_number, marker in stations_to_show.items():
            st = self.stations[st_number]
            mtx[self._y_axis_num_cells - st.map_y][st.map_x - 1].append(marker)

        # print the map
        top_border = '+{}+'.format('-' * (3 * self._x_axis_num_cells + 1))
        print(top_border)
        for y in range(self._y_axis_num_cells):
            print('|', end=' ')
            for x in range(self._x_axis_num_cells):
                # this implementation supports printing up to 3 markers for a single cell
                cell = mtx[y][x]
                print(
                    '{}{}{}'.format(
                        cell[0] if len(cell) > 0 else ' ',
                        cell[1] if len(cell) > 1 else ' ',
                        cell[2] if len(cell) > 2 else ' '),
                    end='')
            print('|')
        print(top_border)

    def _calculate_travel_costs(self):
        # Using Dijkstras algorithm for finding the shortest path
        travel_costs = {
            st_orig: {
                st_dest: {
                    'num_movements': 0 if st_orig == st_dest else math.inf,
                    'previous_st': None,
                    'previous_tr': None,
                }
                for st_dest in self.stations}
            for st_orig in self.stations}

        for st_orig in self.stations:
            d = travel_costs[st_orig]
            unvisited_st_set = set(d.keys())

            while len(unvisited_st_set) > 0:
                st_current = min(
                    unvisited_st_set,
                    key=lambda i: d[i]['num_movements'])
                unvisited_st_set.remove(st_current)

                num_movements_till_here = d[st_current]['num_movements']
                for st_neighbour in self.stations[st_current].get_connected_stations():
                    num_movements_using_current = num_movements_till_here + 1
                    if d[st_neighbour]['num_movements'] > num_movements_using_current:
                        d[st_neighbour]['num_movements'] = num_movements_using_current
                        d[st_neighbour]['previous_st'] = st_current
                        # d[st_neighbour]['previous_tr'] =

        return travel_costs

    def get_num_movements(self, st_orig, st_dest):
        return self.travel_costs[st_orig][st_dest]['num_movements']

    def get_shortest_path(self, st_orig, st_dest):
        path = [st_dest, ]
        while st_dest != st_orig:
            st_dest = self.travel_costs[st_orig][st_dest]['previous_st']
            path.insert(0, st_dest)

        return path


def get_map(version=VERSION_LIST[0], perform_checks=False):
    return _Map(
        version=version,
        perform_checks=perform_checks)
