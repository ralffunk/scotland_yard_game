# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import argparse
from from_detectives_perspective import DetectivesGame, DetectivesAutoThiefGame
# from from_thief_perspective import ThiefGame, ThiefAutoDetectivesGame


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='"Scotland Yard" board game helper')
    parser.add_argument(
        '-d',
        '--detectives',
        action='store_true',
        help='play from the detectives perspective')
    parser.add_argument(
        '-t',
        '--thief',
        action='store_true',
        help='play from the thief perspective')
    parser.add_argument(
        '-a',
        '--auto',
        action='store_true',
        help='make opponent automatic')
    args = parser.parse_args()

    if args.detectives and args.thief:
        raise ValueError('Error: cannot choose "detectives" and "thief" at the same time')

    if args.detectives:
        if args.auto:
            game = DetectivesAutoThiefGame()
        else:
            game = DetectivesGame()
    else:
        if args.auto:
            raise NotImplementedError
            # game = ThiefAutoDetectivesGame()
        else:
            raise NotImplementedError
            # game = ThiefGame()

    game.play()
