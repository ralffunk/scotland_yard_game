# scotland_yard_game

Some code to accompany the board game

---

**Author:** Ralf Funk (ralffunk0@gmail.com)

**Project start date:** december 2018

---

### License

This project is subject to the terms
of the Mozilla Public License, v. 2.0.
If a copy of the MPL was not distributed
with this file, You can obtain one at
http://mozilla.org/MPL/2.0/.
