# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import collections
import datetime
import os
import random
import string
import game_map
from game_map import Transport


class _DetectivesWon(Exception):
    pass


class _ThiefWon(Exception):
    pass


def _now_str():
    # note: includes a pipe at the end
    return datetime.datetime.now().strftime('%Y-%m-%d %H-%M-%S.%f |')


class DetectivesGame:
    PROGRAM_NAME = '"Scotland Yard" board game helper from detectives perspective'
    _DEFAULT_DETECTIVE_NAMES = (
        'R',    # red
        'G',    # green
        'B',    # blue
        'Y',    # yellow
    )
    THIEF_NAME = 'x'
    THIEF_MOVES_AUTOMATICALLY = False

    def __init__(self):
        print(self.PROGRAM_NAME)

        # we do NOT use '_now_str()' here
        self._log_filename = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            'logs',
            '{}-{}.log'.format(
                datetime.datetime.now().strftime('%Y-%m-%dT%H-%M-%S'),
                self.__class__.__name__))

        print()
        print(_now_str(), 'Loading game map ...')
        self.map = game_map.get_map()
        print(
            _now_str(),
            'Game map ready with {} stations'.format(len(self.map.stations)))

        print()
        print(_now_str(), 'Initializing detectives ...')
        self.detectives = collections.OrderedDict()
        self.detectives_movement_counter = 0
        self._create_detectives()
        print(
            _now_str(),
            'Detectives ready {}'.format(
                ' '.join(
                    '[{} {:3d}]'.format(name, st_number)
                    for name, st_number in self.detectives.items())))

        print()
        print(_now_str(), 'Initializing thief ...')
        self.thief_possible_stations = set(self.map.start_stations_for_thief).difference(
            set(self.detectives.values()))
        self.thief_movement_counter = 0
        print(_now_str(), 'Thief ready')

    def log(self, msg):
        with open(self._log_filename, mode='a') as fw:
            fw.write('{} {}\n'.format(_now_str(), msg))

    def _create_detectives(self):
        while True:
            if len(self.detectives) == 0:
                detectives_str = '(no detectives yet)'
            else:
                detectives_str = ' '.join(
                    '[{} {:3d}]'.format(name, st_number)
                    for name, st_number in self.detectives.items())
            print('   Current detectives and their stations: {}'.format(detectives_str))

            while True:
                resp = input(
                    '      (A)dd another detective or (S)tart game '
                    '(and load default detectives if empty): ')
                resp = resp.upper().strip()

                if resp == 'A':
                    # move on the prompt for info on the new detective
                    break
                elif resp in ('S', ''):
                    # finish initialization of detectives

                    if len(self.detectives) == 0:
                        # add default detectives with random stations
                        station_list = list(self.map.start_stations_for_detectives)
                        for name in self._DEFAULT_DETECTIVE_NAMES:
                            i = random.randint(0, len(station_list)-1)
                            self.detectives[name] = station_list.pop(i)

                    return

                print('         Error: "{}" is not a valid answer'.format(resp))

            name = input('      Enter the name of the new detective (only 1 letter): ')
            name = name.upper().strip()
            if len(name) != 1 or name not in string.ascii_uppercase:
                print('         Error: name has to be 1 letter, not "{}"'.format(name))
                continue
            if name in (self.THIEF_NAME, self.THIEF_NAME.upper()):
                # also checking uppercase X
                print('         Error: name cannot be "{}"'.format(self.THIEF_NAME))
                continue
            if name in self.detectives:
                print('         Error: a detective with this name already exists')
                continue

            print('      Valid starting stations: {}'.format(
                self.map.start_stations_for_detectives))
            st_number = input('      Enter the start station of the new detective: ')
            try:
                st_number = int(st_number)
                if st_number not in self.map.start_stations_for_detectives:
                    raise ValueError
            except ValueError:
                print('         Error: "{}" is not a valid start station'.format(
                    st_number))
                continue
            if st_number in self.detectives.values():
                print('         Error: station "{}" is already occupied'.format(
                    st_number))
                continue

            self.detectives[name] = st_number

    def _eval_current_status(self, detectives_stations, thief_possible_stations):
        min_list = []
        avg_list = []
        max_list = []

        # for each detective, calculate how far he is from every possible thief station;
        # here we do NOT yet consider the possible transports each detective has left
        for st_det in detectives_stations:
            num_movements_list = [
                self.map.get_num_movements(st_det, st_thief)
                for st_thief in thief_possible_stations]

            if len(num_movements_list) > 0:
                min_list.append(
                    min(num_movements_list))
                avg_list.append(
                    round(sum(num_movements_list) / len(num_movements_list), 1))
                max_list.append(
                    max(num_movements_list))

        return {
            'min_list_min': min(min_list),
            'min_list_max': max(min_list),
            'avg_list_min': min(avg_list),
            'avg_list_max': max(avg_list),
            'max_list_min': min(max_list),
            'max_list_max': max(max_list),
        }

    def print_current_state(self):
        print()
        print('#' * 63)
        print(_now_str(), 'Displaying current state of the game')
        print('Detectives | made {:2d} moves'.format(
            self.detectives_movement_counter))
        print('           | stations {}'.format(
            ' '.join(
                '[{} {:3d}]'.format(name, st_number)
                for name, st_number in self.detectives.items())))
        print('Thief "{}"  | made {:2d} moves'.format(
            self.THIEF_NAME,
            self.thief_movement_counter))
        print('{:11s}| {} possible stations'.format(
            '(automatic)' if self.THIEF_MOVES_AUTOMATICALLY else '(manual)',
            len(self.thief_possible_stations)))
        print('           | {}'.format(
            tuple(sorted(self.thief_possible_stations))))

        results = self._eval_current_status(
            detectives_stations=self.detectives.values(),
            thief_possible_stations=self.thief_possible_stations)
        print('Distances from stations of detectives to possible stations of the thief:')
        print('   MIN between {:3d}   and {:3d}'.format(
            results['min_list_min'], results['min_list_max']))
        print('   AVG between {:5.1f} and {:5.1f}'.format(
            results['avg_list_min'], results['avg_list_max']))
        print('   MAX between {:3d}   and {:3d}'.format(
            results['max_list_min'], results['max_list_max']))

        d = {}
        for name, st_number in self.detectives.items():
            d[st_number] = name
        for st_number in self.thief_possible_stations:
            d[st_number] = self.THIEF_NAME
        self.map.print_map(d)

    def get_possible_movements_from_station(
            self, st_orig, detectives_stations=None, allowed_tr_set=None):
        if detectives_stations is None:
            detectives_stations = set(self.detectives.values())
        if allowed_tr_set is None:
            allowed_tr_set = set(Transport)

        return tuple(sorted(
            (st_number, tr)
            for st_number, tr_set in self.map.stations[st_orig].connected_stations.items()
            if st_number not in detectives_stations
            for tr in tr_set
            if tr in allowed_tr_set))

    def move_detectives(self):
        print(_now_str(), 'Moving detectives ...')

        movement_data = self._get_detectives_movements()
        self.detectives_movement_counter += 1

        # update detectives stations
        mov_str_list = []
        for name in self.detectives:
            d = movement_data[name]
            self.detectives[name] = d['st_dest']

            mov_str = '"{}" from {:3d} to {:3d} by {}'.format(
                name, d['st_orig'], d['st_dest'], d['tr'].name)
            mov_str_list.append(mov_str)

        print(_now_str(), 'Detectives finished their moves:')
        for mov_str in mov_str_list:
            print(_now_str(), '   {}'.format(mov_str))
        self.log('move_detectives {} ==> {}'.format(
            self.detectives_movement_counter, ' | '.join(mov_str_list)))

        # partial check to see if thief was caught
        # this program does not know the exact location of the thief most of the times, so
        # this part only checks if all possible thief stations have now been occupied by
        # detectives; the thief player has to acknowledge if he was caught even if this
        # program did not know it
        old_thief_stations = set(self.thief_possible_stations)
        self.thief_possible_stations = self.thief_possible_stations.difference(
            set(self.detectives.values()))

        if len(self.thief_possible_stations) == 0:
            raise _DetectivesWon('thief was caught at {}'.format(old_thief_stations))

    def _get_detectives_movements(self):
        movement_data = {}
        new_detectives_stations = set()

        for name, st_orig in self.detectives.items():
            possible_movements = self.get_possible_movements_from_station(
                st_orig, new_detectives_stations)

            if len(possible_movements) == 0:
                raise _ThiefWon('detective "{}" cannot move anymore'.format(name))

            st_dest, tr = self._get_single_detective_movement(
                name, st_orig, possible_movements)
            movement_data[name] = {
                'st_orig': st_orig,
                'st_dest': st_dest,
                'tr': tr,
            }
            new_detectives_stations.add(st_dest)

        return movement_data

    def _get_single_detective_movement(self, name, st_orig, possible_movements):
        print('   Detective "{}" is at station {:3d} and can move:'.format(name, st_orig))
        for i, (st_number, tr) in enumerate(possible_movements):
            print('      {:2d}. to station {:3d} by {}'.format(i+1, st_number, tr.name))

        while True:
            resp = input('   Enter the number of the desired movement: ')
            try:
                resp = int(resp)
                if not (1 <= resp <= len(possible_movements)):
                    raise ValueError

                break
            except ValueError:
                print('      Error: "{}" is not a valid movement option'.format(resp))

        st_dest, tr = possible_movements[resp - 1]
        return st_dest, tr

    def move_thief(self):
        print(_now_str(), 'Moving thief ...')

        possible_transports = set(
            tr
            for st_number in self.thief_possible_stations
            for _, tr in self.get_possible_movements_from_station(st_number))
        possible_transports = tuple(sorted({Transport.ANY, } | possible_transports))
        tr = self._get_thief_transport(possible_transports)
        self.thief_movement_counter += 1

        detectives_set = set(self.detectives.values())
        new_possible_stations = set()
        for st_number in self.thief_possible_stations:
            s = self.map.stations[st_number].get_connected_stations(tr)
            s = s.difference(detectives_set)                    # remove occupied stations
            new_possible_stations.update(s)

        st_dest = None
        if self.thief_movement_counter in self.map.thief_show_rounds:
            # thief needs to tell his new station
            st_dest = self._get_thief_current_station(
                tuple(sorted(new_possible_stations)))

        if st_dest is None:
            self.thief_possible_stations = set(new_possible_stations)
        else:
            self.thief_possible_stations = {st_dest, }

        mov_str = 'Thief moved {} by {} to {}'.format(
            'automatically' if self.THIEF_MOVES_AUTOMATICALLY is None else 'manually',
            tr.name,
            '(secret)' if st_dest is None else st_dest)
        print(_now_str(), mov_str)
        self.log('move_thief {} ==> {}'.format(self.thief_movement_counter, mov_str))

        # assure integrity
        stations = self.thief_possible_stations.difference(set(self.detectives.values()))
        if len(stations) == 0:
            raise _DetectivesWon(
                'apparently the thief moved to a station where a detective was, which '
                'should not have happend')

    def _get_thief_transport(self, possible_transports):
        print('   Thief can (probably) move:')
        for i, tr in enumerate(possible_transports):
            print('      {:2d}. by {}'.format(i+1, tr.name))

        while True:
            resp = input('   Enter the number of the desired transport: ')
            try:
                resp = int(resp)
                if not (1 <= resp <= len(possible_transports)):
                    raise ValueError

                break
            except ValueError:
                print('      Error: "{}" is not a valid transport option'.format(resp))

        tr = possible_transports[resp - 1]
        return tr

    def _get_thief_current_station(self, possible_stations):
        print('   New possible stations of thief: {}'.format(possible_stations))

        while True:
            st_number = input('   Current station of thief: ')
            try:
                st_number = int(st_number)
                if st_number not in possible_stations:
                    raise ValueError

                return st_number
            except ValueError:
                print('      Error: "{}" is not a valid possible station'.format(
                    st_number))

    def play(self):
        print()
        print(_now_str(), 'Starting game ...')

        # this initialization also defines who moves first
        thief_has_next_move = False

        while True:
            self.print_current_state()

            try:
                if thief_has_next_move:
                    if self.thief_movement_counter >= self.map.thief_max_rounds:
                        # the thief already made his last movement; given that detectives
                        # already moved after that and did not win, the thief has won
                        raise _ThiefWon(
                            'thief completed {} rounds without getting caught'.format(
                                self.thief_movement_counter))

                while True:
                    resp = input(
                        '{} (M)ove {} or (E)xit game: '.format(
                            _now_str(),
                            'thief' if thief_has_next_move else 'detectives'))
                    resp = resp.upper().strip()

                    if resp == 'M':
                        # make the next move
                        break
                    elif resp == 'E':
                        print()
                        print(_now_str(), 'Exit game')
                        return

                    print('   Error: "{}" is not a valid answer'.format(resp))

                # make the next move
                if thief_has_next_move:
                    self.move_thief()
                else:
                    self.move_detectives()
            except _DetectivesWon as err:
                self.print_current_state()
                print(_now_str(), 'The detectives won the game: {}'.format(err))
                return
            except _ThiefWon as err:
                self.print_current_state()
                print(_now_str(), 'The thief won the game: {}'.format(err))
                return

            # invert flag
            thief_has_next_move = not thief_has_next_move
