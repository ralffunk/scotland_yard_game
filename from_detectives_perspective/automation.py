# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import random


class AutoThiefMovementMixin:
    THIEF_MOVES_AUTOMATICALLY = True

    def _get_thief_transport(self, possible_transports):
        return random.choice(possible_transports)

    def _get_thief_current_station(self, possible_stations):
        # choose the station that is furthest away from the detectives
        max_num_movements = 0
        best_st = None
        for i, st_thief in enumerate(possible_stations):
            d = {
                st_det: self.map.travel_costs[st_thief][st_det]['num_movements']
                for st_det in self.detectives.values()}
            min_num_movements_for_st_thief = min(d.values())
            if max_num_movements < min_num_movements_for_st_thief:
                max_num_movements = min_num_movements_for_st_thief
                best_st = st_thief

        return best_st
